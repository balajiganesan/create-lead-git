package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class LoginPage extends ProjectMethods {

	public LoginPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(id="username")
	private WebElement eleUsername;
	public LoginPage typeUsername(String data) {
		//WebElement eleUsername = locateElement("id", "username");
		type(eleUsername, data);
		//LoginPage lp = new LoginPage();
		return this;
	}
	@FindBy(id="password")
	private WebElement elePassword;
	public LoginPage typePassword(String data) {
		WebElement elePassword = locateElement("id", "password");
		type(elePassword, data);
		return this;
	}
	@FindBy(className ="decorativeSubmit")
	private WebElement eleLogin;
	public HomePage clickLogin() {
		//WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		return new HomePage();
	}
}





