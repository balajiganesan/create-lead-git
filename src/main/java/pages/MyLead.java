package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MyLead extends ProjectMethods {
	
	public MyLead() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(linkText="Create Lead")
	private WebElement CreateLead;
	public CreateLead clickCreateLeads() {
		click(CreateLead);
		return new CreateLead();
	}

}
