package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class HomePage extends ProjectMethods {

	public HomePage() {
		PageFactory.initElements(driver, this);
	}
/*	@FindBy(className="decorativeSubmit")
	private WebElement eleLogout;
	public LoginPage clickLogout() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleLogout);
		return new LoginPage();
	}*/
		@FindBy()
	private WebElement eleLogout;
	public LoginPage clickLogout() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleLogout);
		return new LoginPage();
	}
	
	@FindBy(linkText="CRM/SFA")
	private WebElement link;
	public MyHomePage clickLinkCRM() {
		click(link);
		return new MyHomePage();
}

}



