package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLead extends ProjectMethods {

	public CreateLead() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="createLeadForm_companyName")
	private WebElement cname;
	public CreateLead typeCname(String data) {
		type(cname, data);
		return this;
	}

	@FindBy(id="createLeadForm_firstName")
	private WebElement fname;
	public CreateLead typefname(String data) {
		type(fname, data);
		return this;
	}
	
	@FindBy(id="createLeadForm_lastName")
	private WebElement lname;
	public CreateLead typelname(String data) {
		type(lname, data);
		return this;
	}
	@FindBy(id="createLeadForm_dataSourceId")
	private WebElement sourceid;
	public CreateLead selectSource(String data) {
		selectDropDownUsingText(sourceid, data);
		return this;
	}
	
	@FindBy(id="createLeadForm_marketingCampaignId")
	private WebElement marketcomp;
	public CreateLead selectMarket(String data) {
		selectDropDownUsingText(marketcomp, data);
		return this;
	}	
	
	@FindBy(className="smallSubmit")
	private WebElement create;
	public ViewLead clickCreate() {
		click(create);
		return new ViewLead();
	}

}
