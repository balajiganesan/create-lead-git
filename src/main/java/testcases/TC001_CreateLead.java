package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC001_CreateLead extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName="TC001_CreateLead";
		testDescription="Creating a new Lead";
		authors="Suresh";
		category="Smoke";
		testNodes ="Leads";
		dataSheetName="TC001";
	}
	@Test(dataProvider="fetchData")
	public void loginAndLogout(String uname, String pwd, String compname, String firstname, String lastname, String source, String market) {
		new LoginPage()
		.typeUsername(uname)
		.typePassword(pwd)
		.clickLogin()
		.clickLinkCRM()
	    .clickLeads()
	    .clickCreateLeads()
	    .typeCname(compname)
		.typefname(firstname)
	    .typelname(lastname)
		.selectSource(source)
		.selectMarket(market)
		.clickCreate()
		.clickLogout();
	}
}
